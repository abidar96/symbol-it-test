import './App.css';
import Excuses from './Components/Excuses';
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';


class App extends Component {
  render() {
    return (
        <Router>
          <Switch>
            <Route path='/' exact={true} component={Excuses}/>
          </Switch>
        </Router>
    )
  }
}
export default App;
