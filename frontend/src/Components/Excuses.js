import Excuse from "./Excuse";
import React, { Component } from 'react';

class Excuses extends Component {
    state = {
      excuses: []
    };
  
    async componentDidMount() {
        
      const response = await fetch('http://localhost:8080/excuses/');      
      const body = await response.json();  
      this.setState({excuses: body});
    }
  
    render() {
      const {excuses} = this.state;
      return (
          <div >           
                {
                    excuses.map(excuse =>
                        <Excuse key={excuse.code} titre={excuse.tag} message={excuse.message}/>
                    )
                }
          </div>
      );
    }
  }
  export default Excuses;