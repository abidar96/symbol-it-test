# Ali abidar & Symbol-it
## Sympble-it coding game test

This is a small springboot project with react for testing my skills in spring boot and java, i also added a little part for react as bonus, it was not required for me.

### Generate doc with javadoc
In the root of project execute this commande

`mvn javadoc:javadoc`

after the end of this commande, you can find the generated files in $ProjectRootDir\target\site\apidocs

### run the spring boot app for api

In the root of project execute this commande
`./mvnw spring-boot:run`

### run reactJs app

In the folder "fron" in the root directory of your project run this command.

`npm start`

For more info, please read Readme file in frontend folder.

Note : maybe you should install npm if if you haven't done it before

then you can access to this page to react app content

`http://localhost:3000`

