package com.example.TestSymbolIt.Controllers;

import java.util.ArrayList;
import java.util.List;
import com.example.TestSymbolIt.DTO.Excuse;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* Excuse controller for fetch and creat excuses
* @author Ali abidar
* 
*/
@CrossOrigin
@RestController
@RequestMapping(path = "/excuses")
public class ExcuseController {

    /**
     * The liste of execuses
     */   
    private List<Excuse> excuses;

    public ExcuseController(){
        this.excuses = new ArrayList<>();
        excuses.add(new Excuse(701,"Inexecusable","Meh"));
        excuses.add(new Excuse(702,"Inexecusable","Emacs"));
        excuses.add(new Excuse(703,"Inexecusable","Explosion"));
        excuses.add(new Excuse(704,"Inexecusable","Goto Fail"));
        excuses.add(new Excuse(705,"Inexecusable","I wrote the code and missed thenecessary validation by an oversight (see 795)"));
        excuses.add(new Excuse(706,"Inexecusable","Delete your account"));
        excuses.add(new Excuse(707,"Inexecusable","Can't quit vi"));
        excuses.add(new Excuse(710,"Inexecusable","PHP"));
        excuses.add(new Excuse(711,"Inexecusable","Convience Store"));
    }

    /**
     * The controller action for getting all excuses 
     * @return the list of excuses
     */
    @GetMapping("/")
    public List<Excuse> GetAllExecuses(){   
        return this.excuses;
    }

    /**
     * The controller action for adding an execuse to the list
     * @param excuse the execuse to be added from the request's body
     * @return the added excuse if it is add successfully, null either
     */
    @PostMapping("/")
    public Excuse addExcuse(@RequestBody Excuse excuse){
        if(this.excuses.add(excuse))
            return excuse;
        return null;
    }
}
