package com.example.TestSymbolIt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestSymbolItApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestSymbolItApplication.class, args);
	}

}
