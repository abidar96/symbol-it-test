package com.example.TestSymbolIt.DTO;

/**
* The excuse's model
* @author Ali abidar
* 
*/
public class Excuse {

    public int code;
    public String tag;
    public String message;
    
    public Excuse(int _code , String _tag, String _message)
    {
        this.code = _code;
        this.tag = _tag;
        this.message = _message;
    }
}
